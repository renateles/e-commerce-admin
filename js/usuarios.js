const APIdotNet = 'https://e-commercesmndotnet.azurewebsites.net',
    APInodeJs = 'https://e-commercenodejs.azurewebsites.net',
    APIseguranca = 'https://ecommercesmn.azurewebsites.net',
    webCliente = 'http://e-commercesmnweb.azurewebsites.net',
    READY_STATE_COMPLETE = 4,   // Valores dos códigos de estado http
    STATUS_OK = 200,
    ACESS_DENIED = 401,
    SERVER_ERROR = 500;
var email, pag, totalFunc,
    perfilModal,
    cadastroModal,
    nome,
    email,
    user,
    pw,
    nomeUsuario,
    tabelaUsuarios,
    totalPaginas,
    itensPagina,
    selectPerm,
    sair,
    formCadastro,
    iconeUsuario,
    flechaLogin,
    avisoCadastro,
    voltarCadastro,
    confirmCadastro,
    avisoEmail,
    avisoUsuario,
    avisoSenha,
    avisoNome,
    idPermissaoEditar,
    getEditarUsuario,
    idFuncionarioEditar,
    order = "ASC",
    by,
    excluirModal;
window.onload = () => {
    var route = `${APIseguranca}/api/public/auth`;
    setRequestGetAuth(estadoAuth, route);

    perfilModal = document.querySelector('#perfilModal');
    cadastroModal = document.querySelector('#cadastroModal');
    excluirModal = document.querySelector('#excluirModal');
    nome = document.querySelector('#nome');
    email = document.querySelector('#email');
    user = document.querySelector('#user');
    pw = document.querySelector('#pw');
    nomeUsuario = localStorage.getItem('nome');
    tabelaUsuarios = document.querySelector('#tabelaUsuarios').tBodies[0];
    totalPaginas = document.querySelector('#totalPaginas');
    itensPagina = document.querySelector('#itensPagina');
    selectPerm = document.querySelector('#selectPerm');
    sair = document.querySelector('#logout');
    formCadastro = document.querySelector('#formCadastro');
    iconeUsuario = document.querySelector('#iconeUsuario');
    flechaLogin = document.querySelector('#flechaLogin');
    avisoCadastro = document.querySelector('#avisoCadastro');
    voltarCadastro = document.querySelector('#voltarCadastro');
    confirmCadastro = document.querySelector('#confirmCadastro');
    avisoEmail = document.querySelector('#avisoEmail');
    avisoUsuario = document.querySelector('#avisoUsuario');
    avisoSenha = document.querySelector('#avisoSenha');
    avisoNome = document.querySelector('#avisoNome');
    iconeUsuario.addEventListener('click', showLogin);
    flechaLogin.addEventListener('click', showLogin);
    sair.addEventListener('click', logout);
    document.getElementById("voltarExcluir").addEventListener("click", function () {
        excluirModal.style.display = 'none';
    });
    voltarCadastro.addEventListener('click', closeModalCadastro)
    email.oninput = () => {
        var regEmail = new RegExp(/^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
        email.value = email.value.toLowerCase();
        if (regEmail.test(email.value)) {
            avisoEmail.innerHTML = "";
        } else {
            avisoEmail.innerHTML = "E-mail inválido";
            return false;
        }
    }
    pw.oninput = () => {
        if ((pw.value.length < 6) || (pw.value.length > 20)) {
            avisoSenha.innerHTML = "<p style='color: red; font-size: 16px;'>A senha deve conter de 6 a 20 caracteres!</p>";
            return false;
        }
        else {
            avisoSenha.innerHTML = "";
        }
    }
    user.oninput = () => {
        if (user.value.indexOf(" ") != -1) {
            document.querySelector('#avisoUsuario').innerHTML = "<p style='color: red; font-size: 16px;'>Não é permitido utilizar espaços no usuário!</p>";
            return false;
        }
        else {
            document.querySelector('#avisoUsuario').innerHTML = "";
        }
    }
    document.querySelector('#welcome').innerHTML = `Bem vindo, <a onclick="showLogin()">${capitalize(nomeUsuario)}</a>`;
    document.querySelector('#userName').innerHTML = `<i class="material-icons md-36">person</i> ${capitalize(nomeUsuario)}`;

    getTabela(1);

    if (window.innerWidth < 600) {
        minimizaMenu();
    }
    else {
        maximizaMenu();
    }
    window.onkeydown = (event) => {
        if (event.keyCode === 27) {
            closeModalCadastro();
            excluirModal.style.display = 'none';
        }
    }
}

window.onresize = () => {
    if (window.innerWidth < 600) {
        minimizaMenu();
    }
    else {
        maximizaMenu();
    }
}
window.onclick = function (event) {
    if (event.target == cadastroModal) {
        closeModalCadastro();
    }
    else if (event.target == perfilModal) {
        perfilModal.style.display = 'none';
    }
    else if (event.target == excluirModal) {
        excluirModal.style.display = 'none';
    }
}
function closeModalCadastro() {
    nome.value = "";
    email.value = "";
    user.value = "";
    pw.value = "";
    avisoNome.innerHTML = "";
    avisoSenha.innerHTML = "";
    avisoEmail.innerHTML = "";
    avisoUsuario.innerHTML = "";
    cadastroModal.style.display = 'none';
    nome.disabled = false;
    email.disabled = false;
    user.disabled = false;
    pw.disabled = false;
    document.getElementById("selectPerm").disabled = false;
    document.getElementById("btnCadastrar").style.display = "block";
    document.getElementById("labelSenha").innerText = "Senha*";
    document.getElementById("tituloModal").innerText = "Cadastro de Usuário";
    document.getElementById("btnCadastrar").innerText = "CADASTRAR";
}
function capitalize(nome) {
    if (nome != null) {
        var nome = nome.split(" ");
        for (var i = 0; i < nome.length; i++) {
            if (nome[i] != "") {
                nome[i] = nome[i][0].toUpperCase() + nome[i].substr(1).toLowerCase();
            }
        }
        if (nome.length > 1) {
            return nome[0] + " " + nome[nome.length - 1];
        }
        else {
            return nome;
        }
    }
}
function showCadastro(funcionario) {
    if (funcionario != undefined) {
        document.getElementById("btnCadastrar").innerText = "EDITAR";
        document.getElementById("labelSenha").innerText = "Nova Senha";
        document.getElementById("tituloModal").innerText = "Editar Usuário";
        pw.required = false;
        if (localStorage.getItem('idPermissao') > funcionario.idpermissao) {
            nome.disabled = true;
            email.disabled = true;
            user.disabled = true;
            pw.disabled = true;
            document.getElementById("selectPerm").disabled = true;
            document.getElementById("btnCadastrar").style.display = "none";
        } else {
            nome.disabled = false;
            email.disabled = false;
            user.disabled = false;
            pw.disabled = false;
            document.getElementById("selectPerm").disabled = false;
            document.getElementById("btnCadastrar").style.display = "block";
        }
    } else {
        getEditarUsuario = false;
    }
    nome.value = funcionario != undefined ? funcionario.nome : "";
    email.value = funcionario != undefined ? funcionario.email : "";
    user.value = funcionario != undefined ? funcionario.usuario : "";
    pw.value = "";
    avisoNome.innerHTML = "";
    avisoSenha.innerHTML = "";
    avisoEmail.innerHTML = "";
    avisoUsuario.innerHTML = "";
    document.getElementById('notificacaoCadastro').className = "";
    document.getElementById('notificacaoCadastro').style.display = "none";
    cadastroModal.style.display = 'flex';
    nome.focus();
    idPermissaoEditar = funcionario != undefined ? funcionario.idpermissao : 2;
    getPermCadastro();
}
function showLogin() {
    perfilModal.style.display = 'block';
}
function ordenaNome(orderProp) {
    by = orderProp;
    if (orderProp == "nome") {
        if (order == "ASC") {
            document.getElementById("arrowNome").innerText = "arrow_drop_up"
        } else {
            document.getElementById("arrowNome").innerText = "arrow_drop_down"
        }
        document.getElementById("arrowEmail").innerText = "arrow_drop_down";
        document.getElementById("arrowUsuario").innerText = "arrow_drop_down";
        document.getElementById("arrowPermissao").innerText = "arrow_drop_down"
    } if (orderProp == "email") {
        if (order == "ASC") {
            document.getElementById("arrowEmail").innerText = "arrow_drop_up"
        } else {
            document.getElementById("arrowEmail").innerText = "arrow_drop_down"
        }
        document.getElementById("arrowNome").innerText = "arrow_drop_down";
        document.getElementById("arrowUsuario").innerText = "arrow_drop_down";
        document.getElementById("arrowPermissao").innerText = "arrow_drop_down"
    } if (orderProp == "usuario") {
        if (order == "ASC") {
            document.getElementById("arrowUsuario").innerText = "arrow_drop_up"
        } else {
            document.getElementById("arrowUsuario").innerText = "arrow_drop_down"
        }
        document.getElementById("arrowNome").innerText = "arrow_drop_down";
        document.getElementById("arrowEmail").innerText = "arrow_drop_down";
        document.getElementById("arrowPermissao").innerText = "arrow_drop_down"
    } if (orderProp == "permissao") {
        if (order == "ASC") {
            document.getElementById("arrowPermissao").innerText = "arrow_drop_up"
        } else {
            document.getElementById("arrowPermissao").innerText = "arrow_drop_down"
        }
        document.getElementById("arrowNome").innerText = "arrow_drop_down"
        document.getElementById("arrowEmail").innerText = "arrow_drop_down";
        document.getElementById("arrowUsuario").innerText = "arrow_drop_down";
    }
    if (order == "ASC") {
        order = "DESC";
    } else {
        order = "ASC";
    }
    getTabela(1);
    mostraLoopUsuario();
}
function limparFiltro() {
    document.getElementById("filtroNome").value = "";
    document.getElementById("filtroEmail").value = "";
    document.getElementById("filtroUsuario").value = "";
    document.getElementById("filtroPerm").value = 0;
    getTabela(1);
}
function getTabela(pagina) {
    let filtroNome = document.getElementById("filtroNome").value,
        filtroEmail = document.getElementById("filtroEmail").value,
        filtroUsuario = document.getElementById("filtroUsuario").value,
        filtroPerm = parseInt(document.getElementById("filtroPerm").value);

    pag = pagina;
    var route = `${APInodeJs}/api/private/funcionario?pagina=${pagina}`;
    // var route = `http://localhost:3002/api/private/funcionario?pagina=${pagina}`;
    if (filtroNome != "")
        route += `&nome=${filtroNome}`;
    if (filtroEmail != "")
        route += `&email=${filtroEmail}`;
    if (filtroUsuario != "")
        route += `&usuario=${filtroUsuario}`;
    if (filtroPerm != 0)
        route += `&permissao=${filtroPerm}`;
    if (by != undefined)
        route += `&by=${by}&order=${order}`;


    setRequestGet(estadoTabela, route);
}
function getPermCadastro() {
    var route = `${APIdotNet}/api/private/permissoes`;

    setRequestGet(estadoCadastro, route);
}
function getEditarFuncionario(id) {
    var route = `${APInodeJs}/api/private/funcionario/${id}`;
    // var route = `http://localhost:3002/api/private/funcionario/${id}`;
    getEditarUsuario = true;
    idFuncionarioEditar = id;
    setRequestGet(getEstadoEditar, route);
    mostraLoopUsuario();
}
function showExcluir(id) {
    excluirModal.style.display = 'flex';
    document.getElementById("btnConfirmarExcluir").setAttribute('onclick', `deleteFuncionario(${id})`);
}
function deleteFuncionario(id) {
    document.getElementById('notificacaoCadastro').className = "";
    document.getElementById('notificacaoCadastro').style.display = "none";
    var route = `${APIdotNet}/api/private/funcionario/${id}`;
    setRequestDelete(estadoDelete, route);
    mostraLoopUsuario();
}
function estadoDelete() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == 200) {
            let json_data = JSON.parse(this.responseText);
            avisoCadastro.innerHTML = `<th style="color: #04BD04;font-size: 15px;">${json_data.message}</th>`;
            animation();
            getTabela(1);
            excluirModal.style.display = 'none';
            fechaLoopUsuario();
        } else if (this.status == 400) {
            let json_data = JSON.parse(this.responseText);
            avisoCadastro.innerHTML = `<th style="color:red;font-size: 15px;">${json_data.message}</th>`;
            animation();
            excluirModal.style.display = 'none';
            fechaLoopUsuario();
        }
    }
}
function getEstadoEditar() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == 200) {
            let json_data = JSON.parse(this.responseText);
            showCadastro(json_data);
            fechaLoopUsuario();
        } else if (this.status == 400) {
            let json_data = JSON.parse(this.responseText);
            avisoCadastro.innerHTML = `<th style="color:red;font-size: 15px;">${json_data.message}</th>`;
            animation();
            fechaLoopUsuario();
        }
    }
}
function estadoSendCadastro() {
    // 0 = estado inicializado
    // 1 = estado carregando
    // 2 = estado já está carregado
    // 3 = estado interativo quando já foi recebido resposta do servidor mas está incompleta
    // 4 = estado completo, já foi recebida a resposta do servidor
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == 201 || this.status == 200) {
            var json_data = JSON.parse(this.responseText);
            avisoCadastro.innerHTML = `<th style="color:#04BD04;font-size: 15px;">${json_data.message}</th>`;
            nome.value = "";
            email.value = "";
            user.value = "";
            pw.value = "";
            document.getElementById("filtroNome").value = "";
            document.getElementById("filtroEmail").value = "";
            document.getElementById("filtroUsuario").value = "";
            document.getElementById("filtroPerm").value = "0";
            order = "ASC";
            getTabela(1);
            closeModalCadastro();
            animation();
        }
        else if (this.status == 400) {
            var json_data = JSON.parse(this.responseText);
            if (json_data.message == 'Email já cadastrado') {
                avisoEmail.innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message == 'Usuário já cadastrado') {
                avisoUsuario.innerHTML = `<p style="color:red">${json_data.message}</p>`;
            } else if (json_data.message == 'Permissão Inválida') {
                closeModalCadastro();
                avisoCadastro.innerHTML = `<th style="color:red;font-size: 15px;">${json_data.message}</th>`;
                animation();
            }
        }
    }
}
function estadoTabela() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == 200) {
            var json_data = JSON.parse(this.responseText);
            var listaUsuarios = "";
            for (var i = 0; i < json_data.dados.funcionarios.length; i++) {
                listaUsuarios += `<tr>
                                    <td onclick='getEditarFuncionario(${json_data.dados.funcionarios[i].id})'>${json_data.dados.funcionarios[i].nome}</td>
                                    <td onclick='getEditarFuncionario(${json_data.dados.funcionarios[i].id})'>${json_data.dados.funcionarios[i].email}</td>
                                    <td onclick='getEditarFuncionario(${json_data.dados.funcionarios[i].id})'>${json_data.dados.funcionarios[i].usuario}</td>
                                    <td onclick='getEditarFuncionario(${json_data.dados.funcionarios[i].id})'>${json_data.dados.funcionarios[i].permissao}</td>
                                    <td onclick='showExcluir(${json_data.dados.funcionarios[i].id})'>
                                            <i class="material-icons deleteHover">
                                                delete_outline
                                            </i>
                                    </td>
                                </tr>`
            }
            tabelaUsuarios.innerHTML = listaUsuarios;
            document.querySelector('.loader').style.display = "none"
            totalFunc = json_data.dados.total;
            totalPaginas.innerHTML = "de " + totalFunc;
            if (pag >= Math.ceil(totalFunc / 10)) {
                document.querySelector('#btnProx').style.opacity = "0.3";
                document.querySelector('#btnProx').style.cursor = "not-allowed";
                itensPagina.innerHTML = `${1 + (pag - 1) * 10} - ${totalFunc}`
            }
            else {
                document.querySelector('#btnProx').style.opacity = "1";
                document.querySelector('#btnProx').style.cursor = "pointer";
                itensPagina.innerHTML = `${1 + (pag - 1) * 10} - ${pag * 10} `
            }
            if (pag == 1) {
                document.querySelector('#btnVoltar').style.opacity = "0.3";
                document.querySelector('#btnVoltar').style.cursor = "not-allowed";
            }
            else {
                document.querySelector('#btnVoltar').style.opacity = "1";
                document.querySelector('#btnVoltar').style.cursor = "pointer";
            }
            fechaLoopUsuario();
        }
        if (this.status == 404) {
            var json_data = JSON.parse(this.responseText);
            var listaUsuarios = `<tr>
                                    <td colspan='5'>${json_data.message}</td>
                                </tr>`;
            tabelaUsuarios.innerHTML = listaUsuarios;
            document.querySelector('#btnVoltar').style.opacity = "0.3";
            document.querySelector('#btnVoltar').style.cursor = "not-allowed";
            document.querySelector('#btnProx').style.opacity = "0.3";
            document.querySelector('#btnProx').style.cursor = "not-allowed";
            itensPagina.innerHTML = `0 - 0`;
        }
    }
}
function estadoCadastro() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            var json_data = JSON.parse(this.responseText);
            var campoPerm = "";
            for (var i = 0; i < json_data.length; i++) {
                if (json_data[i].id == 2) {
                    campoPerm += `<option value="${json_data[i].id}" selected>${json_data[i].nome}</option>`
                }
                else {
                    campoPerm += `<option value="${json_data[i].id}">${json_data[i].nome}</option>`
                }
            }
            selectPerm.innerHTML = campoPerm;
            if (idPermissaoEditar) {
                if (localStorage.getItem('idPermissao') > idPermissaoEditar) {
                    selectPerm.innerHTML = '<option value="1">Administrador</option>';
                }
                document.getElementById("selectPerm").value = idPermissaoEditar;
            }
        }
    }
}
var navegador = () => {
    var nav = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    return nav;
}
var setRequest = (callback, route, data) => {
    var req = new XMLHttpRequest();
    data = JSON.stringify(data);
    req.onreadystatechange = callback;
    req.open('POST', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}
var setRequestEditar = (callback, route, data) => {
    var req = new XMLHttpRequest();
    data = JSON.stringify(data);
    req.onreadystatechange = callback;
    req.open('PUT', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}
var setRequestGet = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}
var setRequestDelete = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('DELETE', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}
function logout() {
    localStorage.removeItem('nome');
    localStorage.removeItem('token');
    window.location.assign(paginaLogin);
}
function prevPgTab() {
    if (pag > 1) {
        getTabela(pag - 1);
    }
}
function nextPgTab() {
    if (pag < Math.ceil(totalFunc / 10)) {
        getTabela(pag + 1);
    }
}

function minimizaMenu() {
    document.querySelector('.menu').style.width = '50px';
    document.querySelector('.menuTitle h2').style.display = 'none';
    document.querySelector('.btnMenu').style.position = 'static';
    document.querySelector('.btnMenu').style.margin = '12px 0 0 0';
    document.querySelector('#iconeUsuarios').style.margin = '0 20px';
    document.querySelector('.conteudo').style.padding = "60px 0 0 50px";
}
function maximizaMenu() {
    document.querySelector('.menu').style.width = '200px';
    document.querySelector('.menuTitle h2').style.display = 'inline-block';
    document.querySelector('.btnMenu').style.position = 'absolute';
    document.querySelector('.btnMenu').style.margin = '25px 10px 0 0';
    document.querySelector('#iconeUsuarios').style.margin = '0 15px';
    document.querySelector('.conteudo').style.padding = "60px 0 0 200px";
}
function btnMenu() {
    if (document.querySelector('.menu').style.width == '50px') {
        maximizaMenu();
    }
    else {
        minimizaMenu();
    }
}


function sendCadastro() {
    var regEmail = new RegExp(/^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
    if ((regEmail.test(email.value)) && ((pw.value.length >= 6) || (pw.value.length <= 20)) && (user.value.indexOf(" ") == -1)) {
        avisoEmail.innerHTML = "";
        avisoUsuario.innerHTML = "";
        var route = `${APIdotNet}/api/private/funcionario`,
            data = {
                'nome': nome.value.toUpperCase().trim(),
                'senha': pw.value,
                'email': document.getElementById('email').value.toLowerCase(),
                'usuario': user.value,
                'idPermissao': parseInt(selectPerm.value)
            };
        if (getEditarUsuario) {
            data.id = idFuncionarioEditar;
            setRequestEditar(estadoSendCadastro, route, data);
            getEditarUsuario = false;
        } else {
            setRequest(estadoSendCadastro, route, data);
        }
        return false;
    }
    else {
        return false;
    }
}
function verifica() {
    var regEmail = new RegExp(/^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
    if (regEmail.test(email.value)) {
        avisoEmail.innerHTML = "";
    } else {
        avisoEmail.innerHTML = "E-mail inválido";
    }
    if (nome.value.replace(/^\s+|\s+$/g, '').length == 0) {
        avisoNome.innerHTML = "<p style='color: red; font-size: 16px;'>Não é permitido espaço na primeira posição do nome!</p>"
    }
    else {
        avisoNome.innerHTML = "";
    }
    if ((pw.value.length < 6) || (pw.value.length > 20)) {
        avisoSenha.innerHTML = "<p style='color: red; font-size: 16px;'>A senha deve conter de 6 a 20 caracteres!</p>";
    }
    else {
        avisoSenha.innerHTML = "";
    }
    if (user.value.indexOf(" ") != -1) {
        avisoUsuario.innerHTML = "<p style='color: red; font-size: 16px;'>Não é permitido utilizar espaços no usuário!</p>";
    }
    else {
        avisoUsuario.innerHTML = "";
    }
}
function animation() {
    document.getElementById('notificacaoCadastro').style.display = "block";
    document.getElementById('notificacaoCadastro').className = "notificacoes";
}