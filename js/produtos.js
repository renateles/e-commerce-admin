const READY_STATE_COMPLETE = 4, STATUS_OK = 200, ACESS_DENIED = 401, SERVER_ERROR = 500,  // Valores dos códigos de estado http   
    linkAzureCliente = 'http://e-commercesmnweb.azurewebsites.net',
    APIdotNet = 'https://e-commercesmndotnet.azurewebsites.net',
    APInodeJs = 'https://e-commercenodejs.azurewebsites.net',
    APIseguranca = 'https://ecommercesmn.azurewebsites.net',
    webCliente = 'http://e-commercesmnweb.azurewebsites.net';
var prodExp,
    imgDisp,
    buscaProd,
    totalPaginas,
    pag,
    btnProx,
    btnVoltar,
    iconeUsuario,
    flechaLogin,
    perfilModal,
    prodModal;  // Armazena informações completas do produto selecionado (modal)
var pegaPreco = document.querySelector("#precoUnitario");
window.onload = () => {
    // Verifica se o token está expirado, inválido ou sem token e redireciona para tela de login 
    var route = 'https://ecommercesmn.azurewebsites.net/api/public/auth';
    setRequestGetAuth(estadoAuth, route);

    perfilModal = document.querySelector('#perfilModal');
    cadastroModalCadastro = document.querySelector('#cadastroModal');
    mostraModal = document.querySelector(".mostra-modal");
    sair = document.querySelector('#logout');
    perfilModal = document.querySelector('#perfilModal');
    iconeUsuario = document.querySelector('#iconeUsuario');
    flechaLogin = document.querySelector('#flechaLogin');
    nomeUsuario = localStorage.getItem('nome');
    iconeUsuario.addEventListener('click', showLogin);
    flechaLogin.addEventListener('click', showLogin);
    sair.addEventListener('click', logout);
    document.querySelector('#welcome').innerHTML = `Bem vindo, <a onclick="showLogin()">${capitalize(nomeUsuario)}</a>`;
    document.querySelector('#userName').innerHTML = `<i class="material-icons md-36">person</i> ${capitalize(nomeUsuario)}`;
    buscaProd = document.querySelector('#buscaProd');
    totalPaginas = document.querySelector('#totalPaginas');
    listarProdutos(1);
    buscaProd.onkeyup = () => {
        setTimeout(listarProdutos(1, buscaProd.value), 1000);
    };
}
// Limpa os atributos nome e token do localStorage
function logout() {
    localStorage.removeItem('nome');
    localStorage.removeItem('token');
    location.href = "login.html";
}
// Mostra login para o usuário deslogar
function showLogin() {
    perfilModal.style.display = 'block';
}
// Criando conteúdo do modal e chamando ele
function modalCadastro() {
    mostraLoop();
    document.querySelector(".mostra-modal").innerHTML = `
    <form onsubmit="return verificaQtd()">
    <div class="container-modal">
    <div class="modal-cadastra-produto">
        <h1>Cadastro de Produto</h1>
        <div class="img-input-lado-lado">
            <div class="img-input-cima-baixo">
                <div>
                    <input type="file" id="i" hidden>
                    <label for="i"><img src="img/camera.png" id="imag" alt="Camera-modal" style="cursor: pointer; margin: 0px 0px 10px 5px;"></label>
                </div>
                <div class="alinha">
                    <div class="input-modal-nome">
                    <div class="msgNome"></div>
                    <div class="msgCampo"></div>
                    <div class="msgNomeSistema"></div>
                        <p>Nome*</p>
                        <input id="nome" type="text" required maxlength="100">
                    </div>
                    <div class="msgQtde"></div>
                    <div class="msgQtdeM"></div>
                    <div class="img-input-lado-lado">
                        <div class="input-tudo-junto">
                        <div class="msgPreco"></div>
                            <p>Preço Un.*</p>
                            <input class="input-modal monetario" required type="text" name="precoUn" id="valor"
                            onKeyPress="return(moeda(this,'.',',',event))" onblur="teste()" onselect="teste2()" placeholder="R$00,00" >
                        </div>
                        <div class="input-tudo-junto">
                            <p>Qtd. Min*</p>
                            <input class="input-modal" required type="number" name="qtde-min" id="qtdeMinima" placeholder="0" min="1">
                        </div>
                        <div class="input-tudo-junto">
                        <div class="msgEntrada"></div>
                            <p>Entrada*</p>
                            <input class="input-modal" required type="number" name="entr" id="qtde" placeholder="0" min="1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="msgImg" style="margin-left: 10px;"></div>
        <div class="input-modal-descricao">
        <div class="msgDesc"></div>
            <p>Descrição*</p>
            <input type="text" required id="descri" maxlength="300">
        </div>
        <div class="btn-modal">
            <button class="btn-c-modal" type="reset" onclick="fechamodal();">CANCELAR</button>
            <button class="btn">CADASTRAR</button>
        </div>
    </div>
</div>
</form>`
    var mascara1 = document.querySelector("#qtdeMinima");
    var mascara2 = document.querySelector("#qtde");
    // Mascara para bloquear as telcas que não sejam numeros nos inputs de quantidade entrada
    function maskInput(evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    }
    mascara1.onkeypress = function (evt) { maskInput(evt) }
    mascara2.onkeypress = function (evt) { maskInput(evt) }
    document.querySelector(".container-modal").style.display = "flex";
    alteraImg();
    fechaLoop();
}

var aux = false;
function teste() {
    aux = false;
}
function teste2() {
    aux = true;
}
// Cria o modal edita e recebe os dados do back-end
function modalEdita() {
    mostraLoop();
    document.querySelector(".mostra-modal").innerHTML = `
    <form onsubmit="return verificaQtdEdita()">
    <div class="container-modal">
    <div class="modal-cadastra-produto">
        <h1>Editar Produto</h1>
        <div class="img-input-lado-lado-edita">
            <div class="img-input-cima-baixo-edita">
                <div>
                    <input type="file" id="i-edita" hidden>
                    <label for="i-edita"><img id="imag" alt="Camera-modal" src="${prodModal.imagem == null ? './img/camera.png' : APInodeJs + '/api/public/produto/img?src=' + prodModal.imagem}" style="cursor: pointer; margin: 0px 0px 10px 5px;">
                </div>
                <div class="alinha">
                    <div class="alinha-codigo">
                        <span class="alinha-codigo-elemento">Código:</span>&nbsp;<span id="codigo-edita" class="alinha-codigo-valor">${prodModal.id}</span>
                    </div>
                    <div class="input-modal-nome-edita">
                    <div class="msgNome"></div>
                    <div class="msgCampo"></div>
                    <div class="msgNomeSistema"></div>
                        <p>Nome*</p>
                        <input type="text" value="${prodModal.nome}" id="nome-edita" required>
                    </div>
                    <div class="msgQtde"></div>
                    <div class="msgQtdeM"></div>
                    <div class="img-input-lado-lado-edita">
                        <div class="alinha-input-preco-qtde">
                            <div class="input-tudo-junto">
                                <div class="msgPreco"></div>
                                <p>Preço Un.*</p>
                                <input class="input-modal monetario" required type="text" name="precoUn" id="preco-unitario-edita" value="R$${toMoney(prodModal.preco)}"
                                onKeyPress="return(moeda(this,'.',',',event))" onblur="teste()" onselect="teste2()" placeholder="R$00,00" >
                            </div>
                            <div class="input-tudo-junto">
                                <p>Qtd. Min*</p>
                                <input class="input-modal" value="${prodModal.quantidademinina}" required type="number" name="qtde-min" id="qtdeM-edita"
                                    placeholder="0" min="1">
                            </div>
                        </div>
                        <div class="alinha-input-entrada-saida-estoque">
                            <div class="input-tudo-junto">
                            <div class="msgEntrada"></div>
                                <p class="cor-input-entrada">Entrada*</p>
                                <input class="input-modal" type="number" name="entrada" id="entrada-edita"
                                    placeholder="0" min="1">
                            </div>
                            <div class="input-tudo-junto">
                                <p class="cor-input-saida">Saída*</p>
                                <input class="input-modal" type="number" name="saida" id="saida-edita"
                                    placeholder="0" min="1"">
                            </div>
                            <div class="input-tudo-junto">
                                <p>Estoque</p>
                                <p>${prodModal.quantidade}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="msgImg" style="margin-left: 10px;"></div>
        <div class="input-modal-descricao-edita">
            <div class="msgDesc"></div>
            <p>Descrição*</p>
            <input type="text" value="${prodModal.descricao}" id="descri-edita" required onclick="alteraImg()">
        </div>
        <div class="btn-modal-edita">
            <button class="btn-c-modal" type="reset" onclick="fechamodal()">CANCELAR</button>
            <button class="btn" onclick="editaProduto()">SALVAR</button>
        </div>
    </div>
</div>
</form>`
    // Mascara para bloquear as telcas que não sejam numeros nos inputs de quantidade entrada e saida
    var mascara1 = document.querySelector("#qtdeM-edita");
    var mascara2 = document.querySelector("#entrada-edita");
    var mascara3 = document.querySelector("#saida-edita");
    function maskInput(evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    }
    mascara1.onkeypress = function (evt) { maskInput(evt) }
    mascara2.onkeypress = function (evt) { maskInput(evt) }
    mascara3.onkeypress = function (evt) { maskInput(evt) }
    // Mostra o conteudo do modal
    document.querySelector(".container-modal").style.display = "flex";
    // pega o arquivo da imagem selecionada
    alteraImgEdita();
    fechaLoop();
}
// Fechando modal
function fechamodal() {
    document.querySelector(".container-modal").style.display = "none";
    document.querySelector(".modal-cadastra-produto").style.display = "none";
}
// Fecha modal com o esc
window.onkeydown = (event) => {
    if (event.keyCode === 27) {
        fechamodal();
    }
}
// Fecha modal ao clicar fora do modal
window.onclick = function (event) {
    if (event.target == document.querySelector(".container-modal")) {
        fechamodal();
    }
    else if (event.target == perfilModal) {
        perfilModal.style.display = 'none';
    }
}
// pega o arquivo da imagem selecionada para o modal de cadastro
function alteraImg() {
    document.querySelector('#i').onchange = function (e) {
        var tgt = e.target || window.event.srcElement,
            files = tgt.files;
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                document.getElementById("imag").src = fr.result;
            }
            fr.readAsDataURL(files[0]);
        }
    }
}
// pega o arquivo da imagem selecionada para o modal de edição de produto
function alteraImgEdita() {
    document.querySelector('#i-edita').onchange = function (e) {
        var tgt = e.target || window.event.srcElement,
            files = tgt.files;
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                document.getElementById("imag").src = fr.result;
            }
            fr.readAsDataURL(files[0]);
        }
    }
}
// Verificando o estado do produto
function estadoSendProduto() {
    // 0 = estado inicializado
    // 1 = estado carregando
    // 2 = estado já está carregado
    // 3 = estado interativo quando já foi recebido resposta do servidor mas está incompleta
    // 4 = estado completo, já foi recebida a resposta do servidor
    if (this.readyState == READY_STATE_COMPLETE) {
        fechaLoop();
        if (this.status == STATUS_OK) {
        }
        else if (this.status == 201) {
            var avisoCadastro = document.querySelector('#avisoCadastro');
            var json_data = JSON.parse(this.responseText);
            avisoCadastro.innerHTML = `<th style="color:#04BD04;font-size: 15px;">${json_data.message}</th>`;
            fechamodal();
            animation();
            listarProdutos(1);
        }
        else if (this.status == 400) {
            var json_data = JSON.parse(this.responseText);
            if (json_data.message == 'Produto já cadastrado') {
                document.querySelector(".msgNome").innerHTML = `<p style="color:red; margin-left: 20px;">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Produto já cadastrado') {
                document.querySelector(".msgNome").innerHTML = "";
            }
            if (json_data.message == 'Produto já cadastrado no sistema, entre em contato com administrador do e-commerce para ativá-lo') {
                document.querySelector(".msgNomeSistema").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Produto já cadastrado no sistema, entre em contato com administrador do e-commerce para ativá-lo') {
                document.querySelector(".msgNomeSistema").innerHTML = "";
            }
            if (json_data.message == 'Quantidade mínima deve ser menor que a entrada') {
                document.querySelector(".msgQtdeM").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Quantidade mínima deve ser menor que a entrada') {
                document.querySelector(".msgQtdeM").innerHTML = "";
            }
            if (json_data.message == 'Campo vazio!') {
                document.querySelector(".msgCampo").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Campo vazio!') {
                document.querySelector(".msgCampo").innerHTML = "";
            }
            if (json_data.message == 'Somente os formatos jpg, jpeg ou png são aceitos') {
                document.querySelector(".msgImg").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Somente os formatos jpg, jpeg ou png são aceitos') {
                document.querySelector(".msgImg").innerHTML = "";
            }
            if (json_data.message == 'Insira um valor') {
                document.querySelector(".msgDesc").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Insira um valor') {
                document.querySelector(".msgDesc").innerHTML = "";
            }
            if (json_data.message == 'Só é permitido números inteiros') {
                document.querySelector(".msgQtde").innerHTML = `<p style="color:red">${json_data.message}</p>`;
            }
            else if (json_data.message != 'Só é permitido números inteiros') {
                document.querySelector(".msgQtde").innerHTML = "";
            }
            return false;
        }
        else if (this.status == 401) {
            fechamodal();
            showLogin();
            return false;
        }
        if (this.status == SERVER_ERROR) {
            document.querySelector("server-erro").innerHTML = `
            <div class="modal" id="erroServidor">   <!-- Modal com mensagem de erro do servidor -->
            <div class="modalErroServer">
                <div class="modal-aviso-erro" id="avisoErro">
                    <header class="cabecalho-modal-server-erro">
                        <h1>Erro - 500</h1>
                    </header>
                    <h2>Ocorreu um erro no servidor</h2>
                    <p>Tente novamente mais tarde</p>
                    <button class="btn-c" id="btnFecha500">FECHAR</button>
                </div>
            </div>
        </div>`;
        }
    }
}
// Metodo de post para envio de cadastro de produto
var setRequest = (callback, route, data) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('POST', route, true);
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}
// Metodo de Alteração para envio do produto editado
var setRequestPut = (callback, route, data) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('PUT', route, true);
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send(data);
}
// Envia cadastro do produto
function sendProduto() { //Resolver problema de espaço para não enviar espaço antes e nem depois
    var formData = new FormData();
    var nome = document.querySelector("#nome").value;
    var preco = document.querySelector("#valor").value.replace(/,/g, "#").replace(/[.]/g, "").replace(/[#]/g, ".").replace("R$", "");
    var qtMinima = document.querySelector("#qtdeMinima").value;
    var qtde = document.querySelector("#qtde").value;
    var desc = document.querySelector("#descri").value;
    var img = document.querySelector("#i").files[0];
    if ((nome.trim() != "") && (preco > 0.01) && (qtMinima % 1 == 0) && (qtde % 1 == 0) && (desc != null) && (desc.trim() != "")) {
        formData.append("nome", nome.trim());
        formData.append("preco", preco);
        formData.append("quantidade", qtde);
        formData.append("quantidademinima", qtMinima);
        formData.append("descricao", desc.trim());
        if (img != undefined) {
            formData.append("imagem", img);
        }
    }
    var route = 'https://e-commercenodejs.azurewebsites.net/api/private/produto';
    // var route = 'http://192.168.52.73:3002'
    if (img != undefined) {
        route += "?hasImage=true";
    }
    setRequest(estadoSendProduto, route, formData);
    document.getElementById('notificacaoCadastro').style.display = "none";
    document.getElementById('notificacaoCadastro').classList.remove('notificacoes');
    mostraLoop();
    return false;
}
// Envia os dados alterados do cadastro do produto
function editaProduto() {
    var formData = new FormData();
    var id = parseInt(document.querySelector("#codigo-edita").textContent);
    var nome = document.querySelector("#nome-edita").value.toLowerCase();
    var preco = document.querySelector("#preco-unitario-edita").value.replace(/,/g, "#").replace(/[.]/g, "").replace(/[#]/g, ".").replace("R$", "");
    var qtMinima = document.querySelector("#qtdeM-edita").value;
    var desc = document.querySelector("#descri-edita").value;
    var img = document.querySelector("#i-edita").files[0];
    var entrada = document.querySelector("#entrada-edita").value == "" ? 0 : parseInt(document.querySelector("#entrada-edita").value);
    var saida = document.querySelector("#saida-edita").value == "" ? 0 : parseInt(document.querySelector("#saida-edita").value);
    if ((nome.trim() != "") && (preco > 0.01) && (qtMinima % 1 == 0) && (desc != null) && (desc.trim() != "")) {
        formData.append("nome", nome.trim());
        formData.append("preco", preco);
        formData.append("quantidade", (entrada - saida));
        formData.append("quantidademinima", qtMinima);
        formData.append("descricao", desc.trim());
        formData.append("id", id);
        if (img != undefined) {
            formData.append("imagem", img);
        }
        else {
            if (prodModal.imagem != null) {
                formData.append("imagem", prodModal.imagem);
            }
        }
    }
    var route = 'https://e-commercenodejs.azurewebsites.net/api/private/produto';
    // var route = 'http://192.168.52.73:3002/api/private/produto'
    // var route = 'http://192.168.52.75:3002/api/private/produto'; //Rota do Josiel
    if (img != undefined) {
        route += "?hasImage=true&src=" + prodModal.imagem;
    }
    setRequestPut(estadoSendProduto, route, formData);
    document.getElementById('notificacaoCadastro').style.display = "none";
    document.getElementById('notificacaoCadastro').classList.remove('notificacoes');
    mostraLoop();
    return false;
}
// Listagem de produtos
function showListaProd() {  // Função que monta a lista de produtos
    listaProd = document.querySelector('#listaProd');
    var aux = "";
    for (var i = 0; i < totalProdLista; i++) {
        if (produtos[i].quantidade > 0) { // Verifica se o produto está disponível
            aux += // Variável para montar a lista de produtos disponíveis
                `<div class="card" id="card-${produtos[i].id}">
            <img src="${produtos[i].imagem == null ? './img/camera.png' : APInodeJs + '/api/public/produto/img?src=' + produtos[i].imagem}"
                alt="Card-img">
            <div class="card-conteudo">
                <div class="card-descricao">
                    <p>${produtos[i].nome}</p>
                    <pre class="card-valor">R$${toMoney(produtos[i].preco)}</pre>
                </div>
                <div class="card-btn">
                    <button class="card-btn-btn" onclick="confirmaDelete(${produtos[i].id})">EXCLUIR</button>&nbsp;
                    <button class="card-btn-btn" onclick="showProduto(${produtos[i].id})">EDITAR</button>
                </div>
            </div>
        </div>`
        }
        else {  // Produto indisponível
            aux += // Variável para montar a lista de produtos indisponíveis
                `<div class="card" id="card-inds-${produtos[i].id}">
            <div class="imgIndisp">
                <img src="${produtos[i].imagem == null ? './img/camera.png' : APInodeJs + '/api/public/produto/img?src=' + produtos[i].imagem}"
                    alt="Card-img-indisponivel">
                <p class="p-indisp">Produto indisponível</p>
            </div>
            <div class="card-conteudo">
                <div class="card-descricao">
                    <p class="alinha-inds-h1">${produtos[i].nome}</p>
                    <pre class="card-valor">R$${toMoney(produtos[i].preco)}</pre>
                </div>
                <div class="card-btn">
                    <button class="card-btn-btn" onclick="confirmaDelete(${produtos[i].id})">EXCLUIR</button>&nbsp;
                    <button class="card-btn-btn" onclick="showProduto(${produtos[i].id})">EDITAR</button>
                </div>
            </div>
        </div>`
        }
    }
    listaProd.innerHTML = aux;
}
// Mostrar Produto para edição
function showProduto(id) {
    var route = `${APIdotNet}/api/private/produto/${id}`;
    setRequestGet(estadoProdCompleto, route);
}
// Verifica o estado do produto para só assim mostra-lo na function ShowProduto
function estadoProdCompleto() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            var json_data = JSON.parse(this.responseText);
            prodModal = json_data;
            modalEdita();
        }
    }
}
// Cria e mostra modal para confirmação de deletar produto
function confirmaDelete(id) {
    document.querySelector(".server-erro").innerHTML = `            
    <div class="aviso-exclui">
    <header class="fundo-cabecalho-modal-exclui">
        <h2>Certeza que deseja excluir?</h2>
    </header>
    <div class="alinha-btn-confirma">
        <button class="btn-confirma-delete" onclick="cancelarDelete()">CANCELAR</button>
        <button class="btn-confirma-delete" onclick="deleta(${id})"">CONFIRMAR</button>
    </div>
</div>`;
    document.querySelector(".server-erro").style.display = "flex"
}
// Envia dados para deletar o card
function deleta(id) {
    var route = `https://e-commercesmndotnet.azurewebsites.net/api/private/produto/${id}`;
    setRequestDelet(estadoProdDelete, route)
    document.getElementById('notificacaoCadastro').style.display = "none";
    document.getElementById('notificacaoCadastro').classList.remove('notificacoes');
    document.querySelector(".server-erro").style.display = "none";
}
// Oculta modal de confimação de deletar pro
function cancelarDelete() {
    document.querySelector(".server-erro").style.display = "none";
}
// Verifica o estado do produto e envia para a rota da API do .NET
var setRequestDelet = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('DELETE', route, true);
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}
// Verficia o estado para fazer o callback e enviar para a API .NET
function estadoProdDelete() {
    if (this.readyState == READY_STATE_COMPLETE) {
        if (this.status == STATUS_OK) {
            var json_data = JSON.parse(this.responseText);
            var avisoCadastro = document.querySelector('#avisoCadastro');
            avisoCadastro.innerHTML = `<th style="color:#04BD04;font-size: 15px;">${json_data.message}</th>`;
            animation();
            listarProdutos(1);
        }
    }
}
// listagem de produtos paginação e filtro
function listarProdutos(pagina, nome) {
    if (nome != null) {
        var route = `https://e-commercesmndotnet.azurewebsites.net/api/private/produto?pagina=${pagina}&nome=${nome}`;
    }
    else {
        var route = `https://e-commercesmndotnet.azurewebsites.net/api/private/produto?pagina=${pagina}&nome=`;
    }
    pag = pagina;
    setRequestGet(estadoProd, route);
}
// Verifica o status do produto para ser listado
function estadoProd() {
    mostraLoop();
    if (this.readyState == READY_STATE_COMPLETE) {
        // Se STATUS_OK mostra a lista de produtos
        if (this.status == STATUS_OK) {
            produtos = [];
            var json_data = JSON.parse(this.responseText);
            for (var i = 0; i < json_data.produtos.length; i++) {
                produtos.push(json_data.produtos[i]);
            }
            totalProdLista = json_data.produtos.length;
            showListaProd();
            fechaLoop();
            totalProd = json_data.total;
            totalPaginas.innerHTML = "de " + totalProd;
            if (pag >= Math.ceil(totalProd / 20)) {
                document.querySelector('#btnProx').style.opacity = "0.3";
                document.querySelector('#btnProx').style.cursor = "not-allowed";
                itensPagina.innerHTML = `${1 + (pag - 1) * 20} - ${totalProd}`
            }
            else {
                document.querySelector('#btnProx').style.opacity = "1";
                document.querySelector('#btnProx').style.cursor = "pointer";
                itensPagina.innerHTML = `${1 + (pag - 1) * 20} - ${pag * 20} `
            }
            if (pag == 1) {
                document.querySelector('#btnVoltar').style.opacity = "0.3";
                document.querySelector('#btnVoltar').style.cursor = "not-allowed";
            }
            else {
                document.querySelector('#btnVoltar').style.opacity = "1";
                document.querySelector('#btnVoltar').style.cursor = "pointer";
            }
            if (json_data.produtos.length == 0) {
                listaProd.innerHTML = `<p style="text-align: center">Produto não encontrado...</p>`;
                itensPagina.innerHTML = `0 - 0`
            }
        }
        if (this.status == SERVER_ERROR) {
            document.querySelector("server-erro").innerHTML = `
            <div class="modal" id="erroServidor">   <!-- Modal com mensagem de erro do servidor -->
            <div class="modalErroServer">
                <div class="modal-aviso-erro" id="avisoErro">
                    <header class="cabecalho-modal-server-erro">
                        <h1>Erro - 500</h1>
                    </header>
                    <h2>Ocorreu um erro no servidor</h2>
                    <p>Tente novamente mais tarde</p>
                    <button class="btn-c" id="btnFecha500">FECHAR</button>
                </div>
            </div>
        </div>`;
        }
    }
}
// Verifica estado dos produtos para Receber os dados da API .NET
var setRequestGet = (callback, route) => {
    var req = meuNavegador();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}
// Verifica se é o IE ou outros para utilização XMLHttpRequest
var meuNavegador = () => {  // internet explorer utiliza ActiveXObject, os outros XMLHttpRequest
    var nav = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    return nav;
}
// Proxima página mandando request da página atual + 1
function prevPgTab() {
    if (pag > 1) {
        if (buscaProd.value != undefined) {
            listarProdutos(pag - 1, buscaProd.value);
        }
        else {
            listarProdutos(pag - 1);
        }
    }
}
// Proxima página mandando request da página atual - 1
function nextPgTab() {
    if (pag < Math.floor(totalProd / 10)) {
        if (buscaProd.value != undefined) {
            listarProdutos(pag + 1, buscaProd.value);
        }
        else {
            listarProdutos(pag + 1);
        }
    }
}
// Verifica quantidade dos valores dos inputs para que a quantidade mínima não exceda o valor da quantidade
function verificaQtd() {
    var qtdeM = document.querySelector("#qtdeMinima");
    var qtde = document.querySelector("#qtde");
    if (qtdeM.length != 0 && qtde.length != 0) {
        if (qtdeM > qtde) {
            document.querySelector(".msgQtde").innerHTML = "Quantidade mínima deve ser menor que entrada";
            return false;
        }
        else if (qtde >= qtdeM) {
            document.querySelector(".msgQtde").innerHTML = "";
        }
        mostraLoop("sendProduto");
        return false;
    }
}
// Verifica quantidade dos valores dos inputs para que a quantidade mínima não exceda o valor da quantidade no modal de editar produto
function verificaQtdEdita() {
    var qtdeM = document.querySelector("#qtdeM-edita");
    var qtde = prodModal.quantidade;
    if (qtdeM.length != 0 && qtde.length != 0) {
        if (qtdeM > qtde) {
            document.querySelector(".msgQtde").innerHTML = "Quantidade mínima deve ser menor que entrada";
            return false;
        }
        else if (qtde >= qtdeM) {
            document.querySelector(".msgQtde").innerHTML = "";
        }
        mostraLoop("editaProduto");
        return false;
    }
}
// Mascara de Real para input de preço
function moeda(a, e, r, t) {
    if (aux) {
        a.value = "";
    }
    if (a.value.length == 12) {
        return false;
    }
    var n = "",
        h = j = 0,
        u = tamanho2 = 0,
        l = ajd2 = "",
        o = window.Event ? t.which : t.keyCode;
    a.value = a.value.replace("R$", "");
    if (13 == o || 8 == o)
        return !0;
    if (n = String.fromCharCode(o),
        -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
        h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++);
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
        0 == (u = l.length) && (a.value = ""),
        1 == u && (a.value = "0" + r + "0" + l),
        2 == u && (a.value = "0" + r + l),
        u > 2) {
        for (ajd2 = "",
            j = 0,
            h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
                j = 0),
                ajd2 += l.charAt(h),
                j++;
        for (a.value = "",
            tamanho2 = ajd2.length,
            h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)

    }
    a.value = "R$" + a.value;
    aux = false;
    return !1
}
// Animação de aviso de cadastro, edita ou deleta produtos
function animation() {
    document.getElementById('notificacaoCadastro').style.display = "block";
    document.getElementById('notificacaoCadastro').className = "notificacoes";
}
// Mascara de . e , nos campos de valor
function toMoney(value) {
    var position, tam, negative = false;
    if (typeof (value) != typeof (3)) {
        return value;
    }
    value = value.toFixed(2);
    value = value.toString();
    value = value.replace('.', ',');
    tam = (value.length);
    if (value.search('-') > -1) {
        negative = true;
        value = value.slice(1, tam);
    }
    // Posição do ponto, quando der 3 casas insere o ponto
    position = (value.length - 3);
    if (tam > 6) {
        while (position - 3 > 0) {
            position -= 3;
            value = value.slice(0, position) + '.' + value.slice(position, tam);
            tam++;
        }
    }
    value = negative ? ('-' + value) : (value);
    return value;
}