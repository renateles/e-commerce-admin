const READY_STATE_COMPLETE = 4,
    STATUS_OK = 200,
    ACCESS_DENIED = 401,
    SERVER_ERROR = 500;
var inputUsuario = document.querySelector("#user");
var paginaInicial = 'http://e-commercesmnwebadmin.azurewebsites.net/home.html';
function sendLogin() {
    var route = 'https://ecommercesmn.azurewebsites.net/api/public/funcionario/login',
        data = {
            'usuario': document.querySelector("#user").value,
            'senha': document.querySelector("#senha").value
        };
    setRequest(estado, route, data);
    mostraLoop();
    return false
}
function estado() {
    if (this.readyState == READY_STATE_COMPLETE) {
        // mostraLoop();
        if (this.status == STATUS_OK) {
            let json_data = JSON.parse(this.responseText);
            localStorage.setItem('token', json_data.token); // guarda o token do usuário no localStorage
            localStorage.setItem('nome', json_data.usuario.nome); // guarda o nome do usuário no localStorage
            localStorage.setItem('idPermissao', json_data.usuario.idpermissao);
            fechaLoop();
            window.location.assign(paginaInicial);
        }
        if (this.status == ACCESS_DENIED) {
            fechaLoop();
            var json_data = JSON.parse(this.responseText);
            document.querySelector(".msg").innerHTML = json_data.message;
        }
        if (this.status == SERVER_ERROR) {
            fechaLoop();
            document.querySelector(".msg").innerHTML = json_data.message;
        }
    }
}

var meuNavegador = () => {  // internet explorer utiliza ActiveXObject, os outros XMLHttpRequest
    var nav = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');// ternário para verificar se é o internet explorer ou outros navegadores
    return nav;
}
var setRequest = (callback, route, data) => {   // (callback = função que irá interagir com a resposta recebida pelo servidor, route = a direção por onde será feita a requisição, data = dados do tipo JSON que será a informação que será enviada para o servidor)
    var req = meuNavegador(); // receberá new XMLHttpRequest() ou new ActiveXObject dependendo do navegador
    data = JSON.stringify(data);
    req.onreadystatechange = callback;  // método onreadystatechange é chamado toda vez que o estado da requisição for alterado
    req.open('POST', route, true); // open = método que irá abrir a conexão com o servidor ; (method = método http pelo qual será enviado a informação, route = rota por onde vai ser enviada a informação, boolean = determina se a requisição vai ser assíncrona ou sincrona)
    req.setRequestHeader('Content-Type', 'application/json');
    req.send(data);
}