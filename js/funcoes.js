var perfilModal,
    nomeUsuario,
    sair,
    iconeUsuario,
    flechaLogin,
    paginaLogin = 'http://e-commercesmnwebadmin.azurewebsites.net/',
    // paginaLogin = "file:///C:/Users/josiel.alves/Desktop/E-Commerce/e-commerce-admin/login.html",
    paginaHome = 'http://e-commercesmnwebadmin.azurewebsites.net/home.html';
    // paginaHome = 'file:///C:/Users/josiel.alves/Desktop/E-Commerce/e-commerce-admin/home.html';

window.onload = () => {
    // Verifica se o token está expirado, inválido ou sem token e redireciona para tela de login 
    var route = 'https://ecommercesmn.azurewebsites.net/api/public/auth';
    setRequestGetAuth(estadoAuth, route);

    perfilModal = document.querySelector('#perfilModal');
    nomeUsuario = localStorage.getItem('nome');
    sair = document.querySelector('#logout');
    iconeUsuario = document.querySelector('#iconeUsuario');
    flechaLogin = document.querySelector('#flechaLogin');
    iconeUsuario.addEventListener('click', showLogin);
    flechaLogin.addEventListener('click', showLogin);
    sair.addEventListener('click', logout);
    document.querySelector('#welcome').innerHTML = `Bem vindo, <a onclick="showLogin()">${capitalize(nomeUsuario)}</a>`;
    document.querySelector('#userName').innerHTML = `<i class="material-icons md-36">person</i> ${capitalize(nomeUsuario)}`;
}

var setRequestGetAuth = (callback, route) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = callback;
    req.open('GET', route, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Access-Control-Allow-Origin', '*');
    req.setRequestHeader('authentication', localStorage.getItem('token'));
    req.send();
}

function estadoAuth() {

    if (this.readyState == 4) {
        mostraLoop();
        if (this.status == 200) {
            if (document.location.href == paginaLogin) {
                window.location.assign(paginaHome);
            }
        }
        fechaLoop();
        if (this.status == 401) {
            var json_data = JSON.parse(this.responseText);
            if (document.location.href != paginaLogin ) {
                if (json_data.message == "Token inválido" || json_data.message == "Token não encontrado ") {
                    logout();
                } else if (json_data.message == "Sessão expirada") {
                    var modalSessaoExpirada =
                        `<div id="sessaoExpiradaModal" class="modal" style="display: flex">
                                <div class="sessao-expirada">
                                    <h2>Sessão Expirada!</h2>
                                    <div class="container-btn-modal-sessao">
                                        <button class="btn btn-voltar" onclick="logout()" id="btnFazerLogin">FAZER LOGIN</button>
                                    </div>
                                </div>
                            </div>`;
                    document.getElementsByTagName("body")[0].innerHTML += modalSessaoExpirada;
                    fechaLoop();
                }
            }
        }
    }
}

window.onclick = function (event) {
    if (event.target == perfilModal) {
        perfilModal.style.display = 'none';
    }
}

function capitalize(nome) {
    if (nome != null) {
        var nome = nome.split(" ");
        for (var i = 0; i < nome.length; i++) {
            nome[i] = nome[i][0].toUpperCase() + nome[i].substr(1).toLowerCase();
        }
        if (nome.length > 1) {
            return nome[0] + " " + nome[nome.length - 1];
        }
        else {
            return nome;
        }
    }
}
function showLogin() {
    perfilModal.style.display = 'block';
}
function logout() {
    localStorage.clear();
    window.location.assign(paginaLogin);
}
// 
window.onresize = function () {
    if (window.innerWidth < 600) {
        minimizaMenu();
    }
    else {
        maximizaMenu();
    }
}
function minimizaMenu() {
    document.querySelector('.conteudo').style.padding = '60px 60px 0px 60px';
    document.querySelector('.menu').style.width = '50px';
    document.querySelector('.menuTitle h2').style.display = 'none';
    document.querySelector('.btnMenu').style.position = 'static';
    document.querySelector('.btnMenu').style.margin = '25px 0 0 0';
    document.querySelector('#iconeUsuarios').style.margin = '0 20px';
}
function maximizaMenu() {
    document.querySelector('.conteudo').style.padding = '60px 60px 0px 225px';
    document.querySelector('.menu').style.width = '200px';
    document.querySelector('.menuTitle h2').style.display = 'inline-block';
    document.querySelector('.btnMenu').style.position = 'absolute';
    document.querySelector('.btnMenu').style.margin = '25px 10px 0 0';
    document.querySelector('#iconeUsuarios').style.margin = '0 15px';
}
function btnMenu() {
    if (document.querySelector('.menu').style.width == '50px') {
        maximizaMenu();
    }
    else {
        minimizaMenu();
    }
}
function mostraLoop(local) {
    document.querySelector(".loop").style.display = "flex";
    if (local == "sendProduto") {
        sendProduto();
    }
}
function fechaLoop() {
    document.querySelector(".loop").style.display = "none";
}
function mostraLoopUsuario() {
    document.querySelector(".loop-usuario").style.display = "flex";
}
function fechaLoopUsuario() {
    document.querySelector(".loop-usuario").style.display = "none";
}